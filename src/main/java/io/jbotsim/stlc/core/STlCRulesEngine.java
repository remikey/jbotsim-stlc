/*
 * Copyright 2019 - 2021, the jbotsim-stlc contributors <david.ilcinkas@labri.fr>
 *
 *
 * This file is part of jbotsim-stlc.
 *
 * jbotsim-stlc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-stlc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-stlc.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package io.jbotsim.stlc.core;

import io.jbotsim.core.Node;

public class STlCRulesEngine {
    private final AbstractSTlCNode myNode;

    public STlCRulesEngine(AbstractSTlCNode node) {
        this.myNode = node;
    }

    public STlCState apply() {
        STlCState state = new STlCState(myNode.getState());

        if(ruleU()) {
            update(state);
        } else if (ruleEB()) {
            state.setStatus(AbstractSTlCNode.STlCStatus.ErrorBroadcast);
        } else if (ruleEF()) {
            state.setStatus(AbstractSTlCNode.STlCStatus.ErrorFeedback);
        } else if (ruleI()) {
            state.setStatus(AbstractSTlCNode.STlCStatus.Isolated);
            state.setParent(null);
        } else if (ruleR()) {
            update(state);
        }
//        else  state = currentState;

        return state;
    }

    private boolean ruleU() {
        boolean active = neighActive() || rootActive();
        return myNode.isCorrectStatus() && active;
    }

    private boolean ruleEB() {
        boolean active = neighActive() || rootActive();
        boolean notRuleU = !active;

        return myNode.isCorrectStatus() && notRuleU && (myNode.abnormalRoot() || myNode.isParentInErrorBroadcast());
    }

    private boolean ruleEF() {
        if (!myNode.isErrorBroadcastStatus())
            return false;

        return myNode.allChildrenInErrorFeedback();
    }

    private boolean ruleI() {
        return !myNode.canBeRoot() && myNode.readyToReset() && myNode.noNeighborInCorrectState();
    }

    private boolean ruleR() {
        boolean appropriateStatus = myNode.isIsolatedStatus() || myNode.readyToReset();
        if (!appropriateStatus)
            return false;

        boolean correctStatusPossible = myNode.canBeRoot() || !myNode.noNeighborInCorrectState();
        return correctStatusPossible;
    }

    /* This definition ensures that neighActive() implies neighValid(). */
    private boolean neighActive() {
        return myNode.neighActiveFilter() && neighValid();
    }
    /* This definition ensures that rootActive() implies rootValid(). */
    private boolean rootActive() {
        return myNode.rootActiveFilter() && rootValid();
    }

    private boolean rootValid() {
        STlCState state = myNode.getState();
        return myNode.canBeRoot() && (state.getDistanceAsRoot().compareTo(state.getDistance())< 0);
    }

    private boolean neighValid() {
        DistNeigh distNeigh = computeDistNeigh();
        if(distNeigh.neighbor == null)
            return false;
        return distNeigh.proposedValue.compareTo(myNode.getState().getDistance())<0;
    }

    private void update(STlCState state) {

        state.setStatus(AbstractSTlCNode.STlCStatus.Correct);

        if(choosesNeighbor()) {
            pickNeighbor(state);
        } else {
            pickSelfAsRoot(state);
        }

    }

    private boolean choosesNeighbor() {
        if (!myNode.canBeRoot())
            return true;

        if (!rootActive() && myNode.isCorrectStatus())
            return true;

        if (!neighActive() && myNode.isCorrectStatus())
            return false;

        return betterNeighborDistance();
    }

    private boolean betterNeighborDistance() {
        DistNeigh distneigh = computeDistNeigh();

        if(distneigh.neighbor == null)
            return false;

        MagmaElement myDistanceAsRoot = myNode.getState().getDistanceAsRoot();
        return distneigh.proposedValue.compareTo(myDistanceAsRoot) <= 0;
    }

    private void pickNeighbor(STlCState state) {
        DistNeigh distNeigh = computeDistNeigh();
        state.setParent(distNeigh.neighbor);
        state.setDistance(distNeigh.proposedValue);
    }

    private void pickSelfAsRoot(STlCState state) {
        state.setParent(null);
        state.setDistance(state.getDistanceAsRoot());
    }

    /**
     * Computes the best distance proposed by a neighbor with status Correct,
     * together with the corresponding neighbor.
     * @return an object DistNeigh, where both neighbor and proposedValue are null when no neighbor has status Correct.
     */
    private DistNeigh computeDistNeigh() {
        DistNeigh distNeigh = new DistNeigh();

        for (Node outNeighbor : myNode.getOutNeighbors()) {
            if(! (outNeighbor instanceof AbstractSTlCNode))
                continue;

            AbstractSTlCNode node = (AbstractSTlCNode) outNeighbor;
            if (node.getState().getStatus() != AbstractSTlCNode.STlCStatus.Correct)
                continue;

            MagmaElement neighborDistance = node.getState().getDistance();
            MagmaElement edgeWeight = myNode.getEdgeWeight(myNode.getOutLinkTo(node));
            MagmaElement myPossibleDistance = neighborDistance.oPlus(edgeWeight);
            if (distNeigh.neighbor == null ||
                    (myPossibleDistance.compareTo(distNeigh.proposedValue) < 0))
                distNeigh.set(node, myPossibleDistance);

        }

        return distNeigh;
    }

    public MagmaElement computeDistanceNeigh() {
        DistNeigh distNeigh = computeDistNeigh();
        return distNeigh.proposedValue;
    }

    private class DistNeigh {
        MagmaElement proposedValue = null;
        AbstractSTlCNode neighbor = null;

        void set(AbstractSTlCNode node, MagmaElement value) {
            proposedValue = value;
            neighbor = node;
        }
    }
}
