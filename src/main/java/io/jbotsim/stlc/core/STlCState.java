/*
 * Copyright 2019 - 2021, the jbotsim-stlc contributors <david.ilcinkas@labri.fr>
 *
 *
 * This file is part of jbotsim-stlc.
 *
 * jbotsim-stlc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-stlc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-stlc.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package io.jbotsim.stlc.core;

public class STlCState {
    private boolean canBeRoot = false;

    private AbstractSTlCNode.STlCStatus status = AbstractSTlCNode.STlCStatus.Isolated;
    private AbstractSTlCNode parent = null;
    private MagmaElement distance;
    private MagmaElement distanceAsRoot;


    public STlCState(STlCState state) {
        canBeRoot = state.canBeRoot;
        status = state.status;
        parent = state.parent;
        distance = state.distance.copy();
        distanceAsRoot = state.distanceAsRoot;
    }

    public STlCState() {

    }


    public boolean canBeRoot() {
        return canBeRoot;
    }

    public void setCanBeRoot(boolean canBeRoot) {
        this.canBeRoot = canBeRoot;
    }

    public AbstractSTlCNode.STlCStatus getStatus() {
        return status;
    }

    public void setStatus(AbstractSTlCNode.STlCStatus status) {
        this.status = status;
    }

    public AbstractSTlCNode getParent() {
        return parent;
    }

    public void setParent(AbstractSTlCNode parent) {
        this.parent = parent;
    }

    public MagmaElement getDistance() {
        return distance;
    }

    public void setDistance(MagmaElement distance) {
        this.distance = distance;
    }


    public MagmaElement getDistanceAsRoot() {
        return distanceAsRoot;
    }

    public void setDistanceAsRoot(MagmaElement distanceAsRoot) {
        this.distanceAsRoot = distanceAsRoot;
    }
}
