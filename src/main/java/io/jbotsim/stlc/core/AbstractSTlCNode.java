/*
 * Copyright 2019 - 2021, the jbotsim-stlc contributors <david.ilcinkas@labri.fr>
 *
 *
 * This file is part of jbotsim-stlc.
 *
 * jbotsim-stlc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-stlc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-stlc.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package io.jbotsim.stlc.core;

import io.jbotsim.core.Link;
import io.jbotsim.core.Node;

public abstract class AbstractSTlCNode extends Node {


    protected STlCState currentState = new STlCState();
    protected STlCState nextState = null;

    private final STlCRulesEngine rulesEngine = new STlCRulesEngine(this);

    public enum STlCStatus {
        Correct,
        ErrorBroadcast,
        ErrorFeedback,
        Isolated
    };

    /**
     * Restricts the cases when a node updates its distance thanks to a neighbor.
     * This method should only depend on this.getState().getDistance() (say d)
     * and on this.computeDistanceNeigh().
     * This method must be monotone with respect to d. More precisely,
     * given d' and d'' with d''>d', if the result is true for d = d',
     * then it is also true for d = d'', all the other parameters being unchanged.
     * @return a boolean, indicating whether the node is allowed to update its distance
     */
    protected abstract boolean neighActiveFilter();

    /**
     * Restricts the cases when a node updates its distance by becoming a root.
     * This method should only depend on the fields canBeRoot, distance (say d),
     * and distanceAsRoot of this.getState().getDistance().
     * This method must be monotone with respect to d. More precisely,
     * given d' and d'' with d''>d', if the result is true for d = d',
     * then it is also true for d = d'', all the other parameters being unchanged.
     * @return a boolean, indicating whether the node is allowed to update its distance
     */
    protected abstract boolean rootActiveFilter();

    /**
     * Provides the weight (say w) of a given link.
     * Note that for any MagmaElement d, we must have d.compareTo(d.oPlus(w)) < 0.
     * @param commonLinkWith a (directed) link incident to this node.
     * @return               the weight (MagmaElement) ot the link
     */
    protected abstract MagmaElement getEdgeWeight(Link commonLinkWith);

    @Override
    public void onClock() {
        super.onClock();

        nextState = computeNextState();
    }

    protected STlCState computeNextState() {
        STlCState nextState = rulesEngine.apply();
        return nextState;
    }

    @Override
    public void onPostClock() {
        super.onPostClock();

        applyNextState();
    }

    private void applyNextState() {
        currentState = nextState;
        nextState = null;
    }

    public STlCState getState() {
        return currentState;
    }

    protected boolean isCorrectStatus() {
        return currentState.getStatus() == STlCStatus.Correct;
    }

    protected boolean isErrorBroadcastStatus() {
        return currentState.getStatus() == STlCStatus.ErrorBroadcast;
    }

    protected boolean isErrorFeedbackStatus() {
        return currentState.getStatus() == STlCStatus.ErrorFeedback;
    }

    protected boolean isIsolatedStatus() {
        return currentState.getStatus() == STlCStatus.Isolated;
    }

    protected boolean abnormalRoot() {
        if (normalRoot())
            return false;

        if (isIsolatedStatus())
            return false;

        return !isNormalStateRegardingParentState(currentState.getParent(), this);
    }

    protected boolean isNormalStateRegardingParentState(AbstractSTlCNode parent, AbstractSTlCNode child) {
        if (parent == null || child == null)
            return false;

        STlCState parentState = parent.getState();
        STlCState childState = child.getState();

        if (childState.getStatus() == STlCStatus.Isolated)
            return false;

        if (parentState.getStatus() == STlCStatus.Isolated)
            return false;

        if (!child.isInNeighborhood(parent))
            return false;

        if (childState.getParent() != parent)
            return false;

        MagmaElement childDistance = childState.getDistance();
        MagmaElement parentDistance = parentState.getDistance();
        MagmaElement parentEdgeWeight = computeParentEdgeWeight(child.getOutLinkTo(parent));
        MagmaElement parentProposedDistance = parentDistance.oPlus(parentEdgeWeight);

        boolean childDistanceGreaterThanProposedByParent = childDistance.compareTo(parentProposedDistance) >= 0;
        boolean sameStatus = childState.getStatus() == parentState.getStatus();
        boolean parentIsErrorBroadcast = parentState.getStatus() == STlCStatus.ErrorBroadcast;

        return childDistanceGreaterThanProposedByParent && (sameStatus || parentIsErrorBroadcast);
    }

    protected boolean noNeighborInCorrectState() {
        for (Node neighbor : getOutNeighbors()) {
            if(! (neighbor instanceof  AbstractSTlCNode))
                continue;
            AbstractSTlCNode node = (AbstractSTlCNode) neighbor;

            if(node.getState().getStatus() == STlCStatus.Correct)
                return false;
        }

        return true;
    }

    protected boolean readyToReset() {
        return isErrorFeedbackStatus() && abnormalRoot();
    }

    protected boolean isInNeighborhood(AbstractSTlCNode node) {
        if (node == null)
            return false;

        return hasOutNeighbor(node);
    }

    protected MagmaElement computeParentEdgeWeight(Link link) {
        return getEdgeWeight(link);
    }

    protected boolean isParentInErrorBroadcast() {
        AbstractSTlCNode parent = currentState.getParent();

        if (!isInNeighborhood(parent)) return false;

        return parent.getState().getStatus() == STlCStatus.ErrorBroadcast;
    }

    protected boolean allChildrenInErrorFeedback() {
        for (Node neighbor : getOutNeighbors()) {
            if(! (neighbor instanceof  AbstractSTlCNode))
                continue;

            AbstractSTlCNode node = (AbstractSTlCNode) neighbor;

            if (isNormalStateRegardingParentState(this, node) &&
                    node.getState().getStatus() != AbstractSTlCNode.STlCStatus.ErrorFeedback)
                return false;

        }

        return true;
    }

    protected boolean normalRoot() {
        boolean distanceIsDistanceAsRoot = currentState.getDistance().compareTo(currentState.getDistanceAsRoot()) == 0;
        boolean noParent = currentState.getParent() == null;
        return canBeRoot() && isCorrectStatus() && noParent && distanceIsDistanceAsRoot;
    }

    protected boolean canBeRoot() {
        return currentState.canBeRoot();
    }

    protected MagmaElement computeDistanceNeigh() {
        return rulesEngine.computeDistanceNeigh();
    }

}
