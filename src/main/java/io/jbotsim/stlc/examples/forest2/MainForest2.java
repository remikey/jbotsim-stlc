/*
 * Copyright 2019 - 2021, the jbotsim-stlc contributors <david.ilcinkas@labri.fr>
 *
 *
 * This file is part of jbotsim-stlc.
 *
 * jbotsim-stlc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-stlc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-stlc.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package io.jbotsim.stlc.examples.forest2;

import io.jbotsim.core.Link;
import io.jbotsim.core.Topology;
import io.jbotsim.stlc.ui.STlCLinkPainter;
import io.jbotsim.ui.JViewer;

public class MainForest2 {

    public static void main(String[] args) {
        System.setProperty("sun.java2d.opengl", "true");
        Topology topology = new Topology(1280,720);
        topology.setTimeUnit(500);
        topology.setDefaultNodeModel(Forest2Node.class);
        topology.setOrientation(Link.Orientation.DIRECTED);
        JViewer jViewer = new JViewer(topology);
        jViewer.getJTopology().setDefaultLinkPainter(new STlCLinkPainter());


        topology.start();
    }

}
