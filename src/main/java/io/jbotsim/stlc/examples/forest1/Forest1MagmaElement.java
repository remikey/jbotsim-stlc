/*
 * Copyright 2019 - 2021, the jbotsim-stlc contributors <david.ilcinkas@labri.fr>
 *
 *
 * This file is part of jbotsim-stlc.
 *
 * jbotsim-stlc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-stlc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-stlc.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package io.jbotsim.stlc.examples.forest1;

import io.jbotsim.stlc.core.MagmaElement;

public class Forest1MagmaElement implements MagmaElement {
    int value = 0;

    public Forest1MagmaElement(int value) {
        this.value = value;
    }

    public Forest1MagmaElement() {

    }

    public Forest1MagmaElement(Forest1MagmaElement element) {
        this(element.value);
    }

    @Override
    public MagmaElement oPlus(MagmaElement right) {
        return new Forest1MagmaElement(value + ((Forest1MagmaElement)right).value);
    }

    @Override
    public MagmaElement copy() {
        return new Forest1MagmaElement(this);
    }

    @Override
    public int compareTo(MagmaElement magmaElement) {
        return value - ((Forest1MagmaElement)magmaElement).value;
    }

    @Override
    public String toString() {
        return value+"";
    }
}
