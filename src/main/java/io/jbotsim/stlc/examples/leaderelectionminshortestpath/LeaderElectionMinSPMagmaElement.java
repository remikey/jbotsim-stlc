/*
 * Copyright 2019 - 2021, the jbotsim-stlc contributors <david.ilcinkas@labri.fr>
 *
 *
 * This file is part of jbotsim-stlc.
 *
 * jbotsim-stlc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-stlc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-stlc.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package io.jbotsim.stlc.examples.leaderelectionminshortestpath;

import io.jbotsim.stlc.core.MagmaElement;

public class LeaderElectionMinSPMagmaElement implements MagmaElement {
    int distance = 0;
    int leaderId = -1;

    protected LeaderElectionMinSPMagmaElement(){

    }
    public LeaderElectionMinSPMagmaElement(int leaderId) {
        this(leaderId, 0);
    }

    public LeaderElectionMinSPMagmaElement(LeaderElectionMinSPMagmaElement element) {
        this(element.leaderId, element.distance);
    }

    public LeaderElectionMinSPMagmaElement(int leaderId, int distance) {
        this.leaderId = leaderId;
        this.distance = distance;
    }

    @Override
    public MagmaElement oPlus(MagmaElement right) {
        LeaderElectionMinSPMagmaElement element = new LeaderElectionMinSPMagmaElement(this.leaderId);
        element.distance = ((LeaderElectionMinSPMagmaElement)right).distance + this.distance;
        return element;
    }

    @Override
    public MagmaElement copy() {
        return new LeaderElectionMinSPMagmaElement(this);
    }

    @Override
    public int compareTo(MagmaElement magmaElement) {
        LeaderElectionMinSPMagmaElement otherElement = (LeaderElectionMinSPMagmaElement) magmaElement;

        if (leaderId < otherElement.leaderId)
            return -1;

        if (leaderId > otherElement.leaderId)
            return 1;

        return this.distance - otherElement.distance;
    }

    @Override
    public String toString() {
        return "("+leaderId+","+distance+")";
    }
}
