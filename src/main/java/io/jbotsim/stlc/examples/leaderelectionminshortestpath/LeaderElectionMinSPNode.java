/*
 * Copyright 2019 - 2021, the jbotsim-stlc contributors <david.ilcinkas@labri.fr>
 *
 *
 * This file is part of jbotsim-stlc.
 *
 * jbotsim-stlc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-stlc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-stlc.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package io.jbotsim.stlc.examples.leaderelectionminshortestpath;

import io.jbotsim.stlc.core.AbstractSTlCNode;
import io.jbotsim.stlc.core.MagmaElement;
import io.jbotsim.core.Color;
import io.jbotsim.core.Link;
import io.jbotsim.ui.icons.Icons;

public class LeaderElectionMinSPNode extends AbstractSTlCNode {
    @Override
    public void onStart() {
        super.onStart();

        getState().setDistance(new LeaderElectionMinSPMagmaElement(-1));
        getState().setDistanceAsRoot(new LeaderElectionMinSPMagmaElement(getID()));
        getState().setCanBeRoot(true);
        getState().setStatus(STlCStatus.Isolated);
        getState().setParent(null);

        //        setDirection(Math.random()*2*Math.PI);
    }

    @Override
    public void onClock() {
        super.onClock();
        updateUI();
//        move(1);
//        wrapLocation();
    }

    private void updateUI() {
        updateColor();

        updateIcon();
    }

    private void updateIcon() {
        if (canBeRoot()) {
            setIcon(Icons.CIRCLE_BLUE_OCEAN_32);
            if (normalRoot())
                setIconSize((int) (DEFAULT_ICON_SIZE * 1.5));
            else
                setIconSize((int) (DEFAULT_ICON_SIZE * 1.2));
        }else {
            setIcon(Icons.DEFAULT_NODE_ICON);
            setIconSize(DEFAULT_ICON_SIZE);
        }
    }

    private void updateColor() {
        switch (getState().getStatus()) {

            case Correct:
                setColor(Color.GREEN);
                break;
            case ErrorBroadcast:
                setColor(Color.ORANGE);
                break;
            case ErrorFeedback:
                setColor(Color.RED);
                break;
            case Isolated:
                setColor(Color.GRAY);
                break;
        }
    }

    public LeaderElectionMinSPNode() {
    }


    @Override
    protected boolean neighActiveFilter() {
        return true;
    }

    @Override
    protected boolean rootActiveFilter() {
        return true;
    }

    @Override
    protected MagmaElement getEdgeWeight(Link commonLinkWith) {
        return new LeaderElectionMinSPMagmaElement(-1, commonLinkWith.getLength().intValue());
    }

    @Override
    public void onSelection() {
        super.onSelection();
        getState().setCanBeRoot(!getState().canBeRoot());
        updateUI();
    }

    @Override
    public String toString() {
        return "Id: "+getID() + ", distance: " + getState().getDistance();
    }
}
