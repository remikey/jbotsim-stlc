/*
 * Copyright 2019 - 2021, the jbotsim-stlc contributors <david.ilcinkas@labri.fr>
 *
 *
 * This file is part of jbotsim-stlc.
 *
 * jbotsim-stlc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-stlc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-stlc.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package io.jbotsim.stlc.examples.rootedshortestpath;

import io.jbotsim.stlc.core.MagmaElement;

public class RSPMagmaElement implements MagmaElement {
    int value = 0;

    public RSPMagmaElement(int value) {
        this.value = value;
    }

    public RSPMagmaElement() {

    }

    public RSPMagmaElement(RSPMagmaElement element) {
        this(element.value);
    }

    @Override
    public MagmaElement oPlus(MagmaElement right) {
        return new RSPMagmaElement(value + ((RSPMagmaElement)right).value);
    }

    @Override
    public MagmaElement copy() {
        return new RSPMagmaElement(this);
    }

    @Override
    public int compareTo(MagmaElement magmaElement) {
        return value - ((RSPMagmaElement)magmaElement).value;
    }

    @Override
    public String toString() {
        return value+"";
    }
}
