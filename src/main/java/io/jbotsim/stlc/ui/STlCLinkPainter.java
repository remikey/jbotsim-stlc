/*
 * Copyright 2019 - 2021, the jbotsim-stlc contributors <david.ilcinkas@labri.fr>
 *
 *
 * This file is part of jbotsim-stlc.
 *
 * jbotsim-stlc is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * jbotsim-stlc is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with jbotsim-stlc.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

package io.jbotsim.stlc.ui;

import io.jbotsim.stlc.core.AbstractSTlCNode;
import io.jbotsim.core.Color;
import io.jbotsim.core.Link;
import io.jbotsim.ui.painting.JDirectedLinkPainter;
import io.jbotsim.ui.painting.UIComponent;

import java.awt.*;

public class STlCLinkPainter extends JDirectedLinkPainter {

    private RenderingHints rh ;

    public STlCLinkPainter() {
        rh =new RenderingHints(
                RenderingHints.KEY_ANTIALIASING,
                RenderingHints.VALUE_ANTIALIAS_ON);
        rh.add(new RenderingHints(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_SPEED));
    }

    @Override
    protected void setRenderingHints(Graphics2D g2d, Link link) {
        g2d.setRenderingHints(rh);
    }

    @Override
    public void paintLink(UIComponent uiComponent, Link link) {
        changeSTlCLinkForDisplay(link);
        super.paintLink(uiComponent, link);
    }

    protected void changeSTlCLinkForDisplay(Link link) {

        if(incorrectNodeTypes(link))
            return;

        AbstractSTlCNode source = (AbstractSTlCNode) link.source;
        AbstractSTlCNode destination = (AbstractSTlCNode) link.destination;
        if (source.getState().getParent() == destination) {
            link.setWidth(Link.DEFAULT_WIDTH * 4);
            link.setColor(Color.GREEN.darker().darker());
        } else {
            if(destination.getState().getParent() == source)
                link.setWidth(0);
            else {
                link.setWidth(Link.DEFAULT_WIDTH);
                link.setColor(Color.GRAY);
            }
        }

    }

    private boolean incorrectNodeTypes(Link link) {
        return !(link.destination instanceof AbstractSTlCNode) || !(link.source instanceof AbstractSTlCNode);
    }

}
