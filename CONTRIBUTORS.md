
## Contributors

The jbotsim-stlc contributors are listed by alphabetical order:

* David Ilcinkas - david.ilcinkas@labri.fr
* Colette Johnen - johnen@labri.fr
* Rémi Laplace - remi.laplace@gmail.com
