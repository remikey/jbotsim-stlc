[![License](https://img.shields.io/badge/license-LGPL%20&ge;%203.0-informational.svg)][lgpl3]

# STlC meta-algorithm on JBotSim



  * [Brief overview](#brief-overview)
  * [Running the STlC meta-algorithm with JBotSim](#running-the-stlc-meta-algorithm-with-jbotsim)
    + [How to use](#how-to-use)
    + [Alternative examples](#alternative-examples)
  * [License](#license)


## Brief overview


This project uses the [JBotSim library][jbotsim-web], a simulation library for distributed algorithms in dynamic networks, 
to implement the [STlC meta-algorithm][STlC-HAL]. 
This meta-algorithm can be instantiated to solve various problems that can be solved by constructing a 
[spanning tree](https://en.wikipedia.org/wiki/Spanning_tree) of a graph.
In particular, it may be used to solve the [Leader Election](https://en.wikipedia.org/wiki/Leader_election), 
the [Minimum Spanning Tree](https://en.wikipedia.org/wiki/Minimum_spanning_tree), or the [Depth-First Search](https://en.wikipedia.org/wiki/Depth-first_search) problems.

The meta-algorithm was designed by the researchers [Stéphane Devismes](http://www-verimag.imag.fr/~devismes/), 
[David Ilcinkas](https://www.labri.fr/perso/ilcinkas/), and [Colette Johnen](https://www.labri.fr/perso/johnen/).
This Java implementation has been developed by Rémi Laplace, research engineer at [LaBRI](https://www.labri.fr),
 with help from David Ilcinkas and Colette Johnen.

## Running the STlC meta-algorithm with JBotSim

The project is designed to work well with [IntelliJ](https://www.jetbrains.com/idea/).
However, to run the application, it suffices to run the following command in a console, at the root of the project:
 
* From Linux, or MacOS: 

```bash
./gradlew run
```

* From Windows: 

```bash
gradlew.bat run
```

### How to use

Examples work in the following manner:
* Add nodes by left-clicking anywhere
* Move nodes around by drag-n-dropping them 
* Remove nodes by right-clicking them
* Toggle the _root_-ability of a node by middle-clicking (or ctrl-shift + left-clicking) them 


### Alternative examples

Multiple examples exist:
* `io.jbotsim.stlc.examples.forest1.MainForest1`
* `io.jbotsim.stlc.examples.forest2.MainForest2`
* `io.jbotsim.stlc.examples.leaderelection.MainLeaderElection`
* `io.jbotsim.stlc.examples.rootedshortestpath.MainRSP`
* `io.jbotsim.stlc.examples.leaderelectionminshortestpath.MainLeaderElectionMinSP`

The command-line project is configured (in `build.gradle`) to run the
 `io.jbotsim.stlc.examples.leaderelection.MainLeaderElection` example.
Please modify this configuration if you want to try another example from the command-line.



## License

(C) Copyright 2019 - 2021, by [the jbotsim-stlc contributors](CONTRIBUTORS.md). All rights reserved.


jbotsim-stlc is published under license [LGPL 3.0 or later][lgpl3]. 


A copy of the license should be available in the product.
The repository also contains a copy of the [GPL](COPYING) and a copy of the [LGPL](COPYING.LESSER).
You can also access copies of the license at <https://www.gnu.org/licenses/>.

SPDX-License-Identifier: LGPL-3.0-or-later



[jbotsim-web]: https://jbotsim.io/
[STlC-HAL]: https://hal.archives-ouvertes.fr/hal-01667863
[lgpl3]: http://www.gnu.org/licenses/lgpl-3.0.html
